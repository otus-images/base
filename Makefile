#!make

REGISTRY := registry.gitlab.com
REGISTRY_PATH := otus-images/base

IMAGES := $(dir $(wildcard */Dockerfile))

$(IMAGES): IMAGE = $(patsubst %/,%,$(dir $@))
$(IMAGES):
	docker build --pull -t ${REGISTRY}/${REGISTRY_PATH}/${IMAGE}:latest -f ${IMAGE}/Dockerfile .
	docker push ${REGISTRY}/${REGISTRY_PATH}/${IMAGE}:latest

.PHONY: $(IMAGES)